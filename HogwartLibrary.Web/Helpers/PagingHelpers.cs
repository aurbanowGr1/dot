﻿using HogwartLibrary.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HogwartLibrary.Web.Helpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper helper, PagingInfo pagingInfo,
            Func<int, string> pageUrl) 
        {
            var builder = new StringBuilder();
            for (int i = 0; i < pagingInfo.TotalPages; i++)
            {
                var tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                tag.AddCssClass("btn btn-primary");
                if (i == pagingInfo.CurrentPage)
                {
                    tag.AddCssClass("btn-danger");
                }
                builder.Append(tag.ToString());
            }
            return MvcHtmlString.Create(builder.ToString());
        }
    }
}