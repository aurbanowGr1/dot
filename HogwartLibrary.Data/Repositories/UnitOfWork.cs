﻿using HogwartLibrary.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogwartLibrary.Data.Repositories
{

    public class UnitOfWork : IDisposable
    {
        private HogwartContext context = new HogwartContext();
        private GenericRepository<Book> _bookRepository;
        private GenericRepository<Category> _categoryRepository;

        public GenericRepository<Book> BookRepository
        {
            get
            {

                if (_bookRepository == null)
                {
                    _bookRepository = new GenericRepository<Book>(context);
                }
                return _bookRepository;
            }
        }

        public GenericRepository<Category> CategoryRepository
        {
            get
            {

                if (_categoryRepository == null)
                {
                    _categoryRepository = new GenericRepository<Category>(context);
                }
                return _categoryRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
