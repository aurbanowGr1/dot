﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogwartLibrary.Data.Models
{
    public class Category
    {
        public Category()
        {
            //Books = new List<Book>();
        }
        public virtual int Id { get; set; }

        [StringLength(30)]
        public virtual string Name { get; set; }
        public virtual ICollection<Book> Books { get; set; }
        public virtual ICollection<Category> Children { get; set; }
        public virtual Category Parent { get; set; }
    }
}
