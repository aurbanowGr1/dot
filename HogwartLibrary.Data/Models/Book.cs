﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HogwartLibrary.Data.Models
{
    public class Book
    {
        public Book()
        {
            //Categories = new List<Category>();
        }
        public virtual int Id { get; set; }

        [MinLength(3)]
        [StringLength(50)]
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        [Required]
        public virtual decimal? Price { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
    }
}
